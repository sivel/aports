# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
pkgver=2022.04.25
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-lang/rust-analyzer"
# limited by rust, x86/armhf/armv7 fail tests
arch="x86_64 aarch64 ppc64le"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="https://github.com/rust-lang/rust-analyzer/archive/$_pkgver/rust-analyzer-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --frozen --release --manifest-path crates/rust-analyzer/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin target/release/rust-analyzer

	install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE-MIT LICENSE-APACHE
	install -Dm644 docs/user/manual.adoc "$pkgdir"/usr/share/doc/$pkgname/manual.adoc
}

sha512sums="
ac35efcdd7094c7667121f7a0b849fb8e2ca8ca36b826f6aea8d3623f434a4ff46c0c69cd728dc75db8b5df463607f8dbe6dffbd96f9ca07caed7e5b4718f519  rust-analyzer-2022.04.25.tar.gz
"
